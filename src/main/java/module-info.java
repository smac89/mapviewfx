module com.smac89.mapviewfx {
    requires javafx.graphics;
    requires java.desktop;

    requires unit.api;
    requires uom.se;
    requires systems.common.java8;
    requires si.units.java8;

    requires org.locationtech.jts;

    requires org.geotools.main;
    requires org.geotools.referencing;
    requires org.geotools.opengis;
    requires org.geotools.render;
    requires org.geotools.wmts;

    requires kotlin.stdlib;
}
