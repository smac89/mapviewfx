package com.smac89.mapviewfx

import javafx.stage.Screen
import org.geotools.geometry.jts.JTS
import org.geotools.geometry.jts.ReferencedEnvelope
import org.geotools.referencing.CRS
import org.geotools.referencing.crs.DefaultGeographicCRS
import org.locationtech.jts.geom.CoordinateXY
import org.opengis.referencing.crs.CoordinateReferenceSystem
import org.opengis.referencing.operation.MathTransform
import si.uom.SI
import systems.uom.common.USCustomary
import tec.uom.se.unit.MetricPrefix
import java.io.Serializable
import javax.measure.Unit
import javax.measure.quantity.Length

object MapUtilFx {
    val METER:     Unit<Length> = SI.METRE
    val KILOMETER: Unit<Length> = MetricPrefix.KILO(METER)
    val FOOT:      Unit<Length> = USCustomary.FOOT
    val MILE:      Unit<Length> = USCustomary.MILE

    // Operating in x/y (meters)
    private val EPSG_3857: CoordinateReferenceSystem by lazy {
        CRS.getAuthorityFactory(true)
            .createCoordinateReferenceSystem("EPSG:3857")
    }

    // (Default) Operating in lat/lng
    private val EPSG_4326: CoordinateReferenceSystem = DefaultGeographicCRS.WGS84

    private val XFORM_4326_TO_3857: MathTransform = CRS.findMathTransform(EPSG_4326, EPSG_3857)

    // A bounding box for viewing the entire globe
    val WORLD_REFERENCE: ReferencedEnvelope by lazy {
        val bbox = CRS.getGeographicBoundingBox(EPSG_4326)
        ReferencedEnvelope(bbox.eastBoundLongitude, bbox.westBoundLongitude, bbox.northBoundLatitude,
            bbox.southBoundLatitude, EPSG_4326)
    }

    private fun CoordinateXY.toLatLon(transform: MathTransform): Coordinate {
        val coord = JTS.transform(this, null, transform)
        return Coordinate(coord.y, coord.x)
    }

    @JvmStatic
    fun screenDPI(): Double = Screen.getPrimary().dpi

    /**
     * Convert lat/lng coordinate to UTM (Universal Transverse Mercator)
     */
    @JvmStatic
    fun lonLatToXY(coordinateLonLat: Coordinate): CoordinateXY =
            lonLatToXY(coordinateLonLat.longitude,
                        coordinateLonLat.latitude)

    /**
     * Convert lat/lng coordinate to UTM (Universal Transverse Mercator)
     */
    @JvmStatic
    fun lonLatToXY(longitude: Double, latitude: Double): CoordinateXY =
            lonLatToXY(org.locationtech.jts.geom.Coordinate(longitude,
                                                             latitude))

    /**
     * Convert lat/lng coordinate to UTM (Universal Transverse Mercator)
     */
    @JvmStatic
    fun lonLatToXY(coordinateLonLat: org.locationtech.jts.geom.Coordinate): CoordinateXY =
            CoordinateXY(JTS.transform(coordinateLonLat, null, XFORM_4326_TO_3857))


    @JvmStatic
    fun xyToLonLat(coordinateXY: CoordinateXY): Coordinate =
            coordinateXY.toLatLon(XFORM_4326_TO_3857.inverse())

    @JvmStatic
    fun xyToLonLat(x: Double, y: Double): Coordinate = xyToLonLat(CoordinateXY(x, y))
}

//data class MapComponents(val latitude: Double, val longitude: Double): Serializable
data class Coordinate(val latitude: Double, val longitude: Double): Serializable

data class Size(val width: Double, val height: Double): Serializable

open class Measurement(val higher: Unit<Length>, val lower: Unit<Length>)
class Imperial: Measurement(MapUtilFx.MILE, MapUtilFx.FOOT)
class Metric: Measurement(MapUtilFx.KILOMETER, MapUtilFx.METER)
