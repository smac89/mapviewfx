package com.smac89.mapviewfx

import javafx.beans.property.ObjectProperty
import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.ReadOnlyObjectProperty
import javafx.beans.value.ObservableDoubleValue
import javafx.scene.canvas.Canvas
import javax.measure.Unit
import javax.measure.quantity.Length

interface MapView {
    fun sizeProperty(): ReadOnlyObjectProperty<Size>

    fun getMap(): IMap

    /**
     * The unit used to represent the scale. The default is [METER][systems.uom.common.USCustomary.METER]
     * @see [MapUtilFx] for available units
     *
     * To ensure compatibility, any units used should be convertible to
     * [INCH][systems.uom.common.USCustomary.INCH]
     */
    fun scaleUnitProperty(): ObjectProperty<Unit<Length>>

    /**
     * Corresponds to the size of pixel in relation to the current bounds of the map
     * The unit of the scale depends on [scaleUnitProperty]
     */
    fun scaleProperty(): ObservableDoubleValue

    fun initializedProperty(): ReadOnlyBooleanProperty
    fun initialize()

    companion object {
        @JvmStatic
        fun create(): MapView = MapViewFX()
    }
}

open class ResizableCanvas: Canvas() {
    override fun isResizable() = true
    override fun minWidth(height: Double) = 0.0
    override fun minHeight(width: Double) = 0.0
    override fun maxWidth(height: Double) = Double.MAX_VALUE
    override fun maxHeight(width: Double) = Double.MAX_VALUE
    override fun resize(width: Double, height: Double) {
        this.width = width
        this.height = height
    }
}
