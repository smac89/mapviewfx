package com.smac89.mapviewfx

import com.smac89.mapviewfx.MapUtilFx.WORLD_REFERENCE
import com.smac89.mapviewfx.event.MapDragDetector
import com.smac89.mapviewfx.event.MapDragEvent
import com.smac89.mapviewfx.event.MapEvent
import com.smac89.mapviewfx.layer.IMapLayerFX
import com.smac89.mapviewfx.layer.MapLayerEventType
import com.smac89.mapviewfx.tile.Tile
import javafx.animation.PauseTransition
import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanBinding
import javafx.beans.binding.DoubleBinding
import javafx.beans.property.*
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.input.ZoomEvent
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.util.Duration
import org.geotools.geometry.DirectPosition2D
import org.geotools.geometry.jts.ReferencedEnvelope
import org.geotools.map.MapViewport
import org.geotools.ows.wmts.WebMapTileServer
import org.geotools.referencing.CRS
import org.geotools.referencing.GeodeticCalculator
import systems.uom.common.USCustomary
import java.awt.Rectangle
import java.lang.System.Logger.Level.INFO
import java.net.URL
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.pow

class MapViewFX:
        Region(), MapView, EventHandler<MapEvent> by MapEventsHandler() {

    private val layersView = StackPane()
    private val layersViewClip = javafx.scene.shape.Rectangle(width, height)

    private val map = MapFX()

    // http://geojson.io/
    private val mapViewport = MapViewport(WORLD_REFERENCE, true)
    private val mapServer = WebMapTileServer(URL(WMTS_URL))

    private val logger = System.getLogger(javaClass.canonicalName)

    private val resizedProperty = ReadOnlyObjectWrapper<Size>(this, "size")
    private val initializedProperty = ReadOnlyBooleanWrapper(this, "initialize", false)
    private val scaleUnitProperty = SimpleObjectProperty(this, "measure", MapUtilFx.METER)

    private val scaleProperty: DoubleBinding
    private val repaintProperty: BooleanBinding

    private val mapDragDetector: MapDragDetector

    init {
        super.getChildren().add(layersView)

        layersViewClip.widthProperty().bind(widthProperty())
        layersViewClip.heightProperty().bind(heightProperty())

        scaleProperty = Bindings.createDoubleBinding({
            calculateScale(Tile.DEFAULT_TILE_SIZE)
        }, arrayOf(scaleUnitProperty, map.zoomLevelProperty(), map.centerProperty()))

        repaintProperty = Bindings.createBooleanBinding({ true }, arrayOf(
            map.centerProperty(),
            map.zoomLevelProperty(),
            resizedProperty,
            initializedProperty,
            map))

        mapDragDetector = MapDragDetector(this)
    }

    override fun getMap(): IMap = map

    override fun sizeProperty(): ReadOnlyObjectProperty<Size> = resizedProperty.readOnlyProperty

    override fun scaleUnitProperty() = scaleUnitProperty
    override fun scaleProperty() = scaleProperty
    override fun initializedProperty(): ReadOnlyBooleanProperty = initializedProperty.readOnlyProperty

    /**
     * Initialize and setup the screen area as well as the
     */
    override fun initialize() {
        Platform.runLater {
            if (initializedProperty().value) {
                throw IllegalStateException("Map has already been initialized!")
            }

            // Property listeners
            initListeners()

            // Handle events like dragging the map, etc
            initViewEventsHandling()

            // Handle repainting the canvas
            initPicassoMode()

            logger.log(INFO, "World reference: ${WORLD_REFERENCE.centre()}")

//            mapServer.capabilities.layerList

//            this.map.addMapLayer(WMTSMapLayer(mapServer.capabilities.getLayer("osm")))

            initializedProperty.set(true)
        }
    }

    /**
     * Initialize handling of different events
     */
    private fun initViewEventsHandling() {
        addEventHandler(MapEvent.MAP_DRAG_EVENT, this::handleMapDrag)
        addEventHandler(ZoomEvent.ANY) {
            logger.log(INFO, "Zoom ${it.zoomFactor}")
        }
    }

    /**
     * Listen for property changes
     */
    private fun initListeners() {
        map.centerProperty().addListener { _, _, coordinate ->
            if (initializedProperty().value) {
                val worldArea = getBoundingBoxForCoordinate(coordinate)
                updateWorldArea(worldArea)
            }
        }

        resizedProperty.addListener { _, _, size ->
            mapViewport.screenArea = Rectangle(size.width.toInt(), size.height.toInt())

//            layersView.width = size.width + 256 * 2
//            layersView.height = size.height + 256 * 2

            logger.log(INFO, "screen area: ${mapViewport.screenArea}")
        }

        map.addMapLayerListener(MapLayerEventType.ALL) { eventType, layer ->
            when (eventType) {
                MapLayerEventType.LAYER_ADDED -> {
                    if (layer is IMapLayerFX)
                        layersView.children.add(layer.getFXView())
                }
                MapLayerEventType.LAYER_REMOVED -> {
                    if (layer is IMapLayerFX) {
                        layersView.children.remove(layer.getFXView())
                    }
                }
                else -> {
                }
            }
        }
    }

    /**
     * Initialize listeners to know when the map needs to be repainted
     */
    private fun initPicassoMode() {
        mapViewport.addMapBoundsListener {
            logger.log(INFO, "Previous map bounds: ${it.oldAreaOfInterest}")
            logger.log(INFO, "New map bounds: ${it.newAreaOfInterest}")
            repaintProperty.invalidate()
            if (it.newCoordinateReferenceSystem != it.oldCoordinateReferenceSystem) {
                scaleProperty.invalidate()
            }
        }

        // Painting should be delayed by 100ms before starting
        // This reduces loss of events
        val repaintTimer = PauseTransition(Duration.millis(100.0))

        repaintProperty.addListener { _ ->
            logger.log(INFO, "Update the tiles!")
            if (repaintProperty.value) {
                repaintTimer.onFinished = EventHandler{ drawTiles() }
                repaintTimer.playFromStart()
            }
        }
    }

    /**
     * Move the viewport to focus on the new position of the map
     * @param event The drag event
     */
    private fun handleMapDrag(event: MapDragEvent) {
        logger.log(INFO,
            "Dragged canvas for offSetX: ${event.dragOffsetX}, offSetY: ${event.dragOffsetY}")
        val bounds = mapViewport.bounds // copy

        val newPos = DirectPosition2D(event.dragOffsetX, event.dragOffsetY)
        val result = mapViewport.screenToWorld.transform(newPos, null)
        bounds.translate(bounds.minX - result.x, bounds.maxY - result.y)

        updateWorldArea(bounds)
    }

    /**
     * Updates the visible world area and repaints the canvas
     * @param visibleWorldArea The bounding box to show of the world
     */
    private fun updateWorldArea(visibleWorldArea: ReferencedEnvelope) {
        mapViewport.bounds = visibleWorldArea
    }

    /**
     * Retrieves the tiles for this layer which are visible within the current view
     * @param mapLayer The layer to get tiles for
     * @return The tiles for the given layer
     */
//    private fun getTiles(mapLayer: WMTSLayer): Set<org.geotools.tile.Tile> {
//        logger.log(INFO, "Getting tiles. width: $width, height: $height")
//        val tileRequest = mapServer.createGetTileRequest().apply {
//            this.setLayer(mapLayer)
//            this.setRequestedHeight(height.toInt())
//            this.setRequestedWidth(width.toInt())
//            this.setCRS(mapLayer.preferredCRS)
//            this.setRequestedBBox(mapViewport.bounds)
//        }
//        return mapServer.issueRequest(tileRequest)
//    }

    /**
     * Draws all the tiles which are supposed to be visible
     * within the current world bounds
     */
    private fun drawTiles() {
        if (mapViewport.isEmpty) {
            return
        }

        logger.log(INFO, "Drawing layers!!")
        for (layer in map.getMapLayers()) {
            if (layer.visibleProperty().value) {
                layer.render()
            }
//            val visibleTiles = getTiles(layer).sortedWith(
//                compareBy({it.tileIdentifier.y}, {it.tileIdentifier.x})
//            )

//            for (tile in visibleTiles) {
//                logger.log(INFO, tile.tileSize)
//                val tileImage = Image(tile.url.toExternalForm(),
//                    tile.tileSize.toDouble(), tile.tileSize.toDouble(), true, true, true)
//                val imgView = ImageView()
//                tileImage.progressProperty().addListener { _, _, progress ->
//                    if (progress.toInt() == 1) {
//                        imgView.image = tileImage
//                        logger.log(INFO, tileImage.height)
//                        logger.log(INFO, tileImage.width)
//                    }
//                }
//                break
//            }
        }

//        val currentScale = RendererUtilities.calculateScale(
//            mapViewport.bounds,
//            width.toInt(),
//            height.toInt(),
//            MapUtilFx.screenDPI()
//        )
//
//        logger.log(INFO, "Current scale: $currentScale")
//        logger.log(INFO, "Current OGC scale: ${RendererUtilities.calculateOGCScaleAffine(mapViewport.coordinateReferenceSystem,
//            mapViewport.worldToScreen,
//            mapOf("dpi" to MapUtilFx.screenDPI()))}")
//        logger.log(INFO, "Current view bounds: ${mapViewport.bounds}")
//
//        val zoomLevelMatcher = ScaleZoomLevelMatcher.createMatcher(
//            mapViewport.bounds, mapViewport.coordinateReferenceSystem, currentScale)
//
//        val zoom = zoomLevelMatcher.getZoomLevelFromScale(null, doubleArrayOf())
    }

    /**
     * Calculate the scale of the map at a given zoom level
     * Taken from [https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale][Osm wiki]
     * @param tileSize The size of a tile
     * @return The map scale at the given zoom level and latitude
     */
    private fun calculateScale(tileSize: Double): Double {
        val mapScaleUnit = scaleUnitProperty.value
        val radius = CRS.getEllipsoid(mapViewport.coordinateReferenceSystem).semiMajorAxis
        val resolution = (radius * 2 * PI / tileSize) *
                         cos(map.getCenterLatLon().latitude) / 2.0.pow(map.getZoomLevel())

        val inchConverter = USCustomary.INCH.getConverterTo(mapScaleUnit)
        val meterConverter = USCustomary.METER.getConverterTo(mapScaleUnit)

        return MapUtilFx.screenDPI() * inchConverter.convert(1.0) * meterConverter.convert(resolution)
    }

    /**
     * Calculates a bounding box around a given coordinate
     * @param coordinate The coordinate to center the box around
     * @return A bounding box that has a width equal to the
     */
    private fun getBoundingBoxForCoordinate(coordinate: Coordinate): ReferencedEnvelope {
        val currentBounds = mapViewport.bounds
        val maxWidth = mapViewport.bounds.width
        val maxDistance = maxWidth / (2 * cos(45.0))

        val calculator = GeodeticCalculator(mapViewport.bounds.coordinateReferenceSystem)

        val azimuthTop = -45.0
        calculator.setStartingGeographicPoint(currentBounds.centre().x, currentBounds.centre().y)
        calculator.setDestinationGeographicPoint(coordinate.longitude, coordinate.latitude)

        calculator.setDirection(azimuthTop, maxDistance)
        val destTop = calculator.destinationGeographicPoint

        val azimuthBottom = 135.0
        calculator.setStartingGeographicPoint(coordinate.longitude, coordinate.latitude)
        calculator.setDirection(azimuthBottom, maxDistance)
        val destBottom = calculator.destinationGeographicPoint

        // World rectangle has origin at LLC (Lower Left Corner)
        // See https://docs.geotools.org/latest/userguide/tutorial/affinetransform.html#definitions
        return ReferencedEnvelope(destTop.x, destBottom.x, destTop.y, destBottom.y,
            mapViewport.bounds.coordinateReferenceSystem)
    }

    override fun getChildren(): ObservableList<Node> {
        return childrenUnmodifiable
    }

    override fun layoutChildren() {
        layersView.autosize()
    }

    override fun resize(width: Double, height: Double) {
        super.resize(width, height)
        resizedProperty.set(Size(width, height))
    }

    companion object {
        private val DEFAULT_CENTER_COORDINATE = Coordinate(52.1332, -106.6700)
        private const val OSM_BASE_URL = "http://vmi264557.contaboserver.net:8885/tiles/osm/osm_grid/"
        private const val WMTS_URL = "http://vmi264557.contaboserver.net:8885/wmts/1.0.0/WMTSCapabilities.xml"


//        fun scaleToBBox(scaleX: Double, scaleY: Double = scaleX): ReferencedEnvelope {
//            val width = WORLD_REFERENCE.width / scaleX
//            val height = WORLD_REFERENCE.height / scaleY
//
//            return getBoundingBoxForCoordinate(DEFAULT_CENTER_COORDINATE,
//                maxHeight = height,
//                maxWidth = width).transform(EPSG_3857, false)
//        }
    }
}
