package com.smac89.mapviewfx.event

import javafx.event.Event
import javafx.event.EventType
import javafx.scene.Node
import javafx.scene.input.MouseDragEvent
import javafx.scene.input.MouseEvent
import java.lang.ref.WeakReference

/**
 * Class used to detect drag events on a node
 */
class MapDragDetector(map: Node) {
    private val map: WeakReference<Node> = WeakReference(map)
    private var orgSceneX: Double? = null
    private var orgSceneY: Double? = null

    init {
        this.map.get()?.let {
            it.addEventHandler(MouseEvent.DRAG_DETECTED, ::handleMousePressed)
            it.addEventHandler(MouseDragEvent.MOUSE_DRAG_OVER, ::handleMouseDragged)
            it.addEventHandler(MouseDragEvent.MOUSE_DRAG_RELEASED, ::handleMouseReleased)
        }
    }

    private fun handleMouseDragged(event: MouseEvent) {
        if (orgSceneX != null && orgSceneY != null) {
            map.get()?.let {
                Event.fireEvent(it,
                    MapDragEvent(event.source as Node,
                        event.sceneX - orgSceneX!!,
                        event.sceneY - orgSceneY!!)
                )
            }
            orgSceneX = event.sceneX
            orgSceneY = event.sceneY
            event.consume()
        }
    }

    private fun handleMousePressed(event: MouseEvent) {
        map.get()?.startFullDrag()
        orgSceneX = event.sceneX
        orgSceneY = event.sceneY
        event.consume()
    }

    private fun handleMouseReleased(event: MouseEvent) {
        orgSceneX = null
        orgSceneY = null
        event.consume()
    }
}

data class MapDragEvent(val source: Node, val dragOffsetX: Double, val dragOffsetY: Double):
        MapEvent(source,
            MAP_DRAG_EVENT
        )
data class MapZoomEvent(val source: Node):
        MapEvent(source,
            MAP_ZOOM_EVENT
        )

sealed class MapEvent(source: Node,
                      eventType: EventType<out MapEvent>): Event(source, source, eventType) {
    companion object {
        val ANY = EventType<MapEvent>("MAP_EVENT")
        val MAP_DRAG_EVENT = EventType<MapDragEvent>(
            ANY, "MAP_DRAG_EVENT")
        val MAP_ZOOM_EVENT = EventType<MapZoomEvent>(
            ANY, "MAP_ZOOM_EVENT")
    }
}
