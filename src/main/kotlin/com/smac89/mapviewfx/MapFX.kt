package com.smac89.mapviewfx

import com.smac89.mapviewfx.layer.IMapLayer
import com.smac89.mapviewfx.layer.MapLayerEventType
import com.smac89.mapviewfx.layer.MapLayerListener
import javafx.beans.InvalidationListener
import javafx.beans.Observable
import javafx.beans.property.ReadOnlyDoubleProperty
import javafx.beans.property.ReadOnlyDoubleWrapper
import javafx.beans.property.ReadOnlyObjectProperty
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener.Change
import javafx.collections.ObservableList
import java.util.*
import kotlin.math.abs

internal class MapFX(private val observers: ObservableList<InvalidationListener> = FXCollections.observableArrayList()) :
        IMap, Observable by observers {

    private val mapLayers = FXCollections.observableArrayList<IMapLayer>()
    private val mapLayerListeners = EnumMap<MapLayerEventType, MutableList<MapLayerListener>>(
        MapLayerEventType::class.java)

    private val zoomLevel = ReadOnlyDoubleWrapper(this, "zoomLevel", 0.0)
    private val zoomStep = ReadOnlyDoubleWrapper(this, "zoomStep", 0.1)
    private val maxZoom = ReadOnlyDoubleWrapper(this, "maxZoom", 18.0)
    private val minZoom = ReadOnlyDoubleWrapper(this, "minZoom", 0.0)

    private val center = ReadOnlyObjectWrapper(this, "center", Coordinate(0.0, 0.0))

    init {
        mapLayers.addListener(InvalidationListener { fireMapInvalidated() })

        mapLayers.addListener { change: Change<out IMapLayer> ->
            if (change.next()) {
                do {
                    if (change.wasAdded()) {
                        val addedLayer = change.addedSubList[0]
                        val eventType = MapLayerEventType.LAYER_ADDED
                        mapLayerListeners[eventType]?.forEach {
                            it.layerEvent(eventType, addedLayer)
                        }
                    } else if (change.wasRemoved()) {
                        val removedLayer = change.removed[0]
                        val eventType = MapLayerEventType.LAYER_REMOVED
                        mapLayerListeners[eventType]?.forEach {
                            it.layerEvent(eventType, removedLayer)
                        }
                    }
                } while (change.next())
            }
        }
    }

    override fun getCenterLatLon(): Coordinate = center.value

    override fun getZoomLevel(): Double = zoomLevel.value
    override fun getMaxZoomLevel(): Double = maxZoom.value
    override fun getMinZoomLevel(): Double = minZoom.value
    override fun getZoomStep(): Double = zoomStep.value

    override fun setCenterLatLon(coordinate: Coordinate) {
        center.set(coordinate)
    }

    override fun setZoomLevel(zoomLevel: Double) {
        if (zoomLevel <= maxZoom.value && zoomLevel >= minZoom.value) {
            this.zoomLevel.set(zoomLevel)
        } else throw IllegalArgumentException(
            "zoomLevel ($zoomLevel) should be in the range [${minZoom.value}, ${maxZoom.value}]")
    }

    override fun setMaxZoomLevel(maxZoom: Double) {
        if (maxZoom > minZoom.value) {
            this.maxZoom.set(maxZoom)
        } else throw IllegalArgumentException(
            "maxZoom ($maxZoom) should be greater than minZoom (${minZoom.value})")
    }

    override fun setMinZoomLevel(minZoom: Double) {
        if (minZoom < maxZoom.value) {
            this.minZoom.set(minZoom)
        } else throw IllegalArgumentException(
            "minZoom ($minZoom) should be less than maxZoom (${maxZoom.value})")
    }

    override fun setZoomStep(zoomStep: Double) {
        this.zoomStep.set(abs(zoomStep))
    }

    override fun zoomIn() {
        val nextZoom = zoomLevel.value + zoomStep.value
        if (nextZoom < maxZoom.value) {
            zoomLevel.set(nextZoom)
        }
    }

    override fun zoomOut() {
        val nextZoom = zoomLevel.value - zoomStep.value
        if (nextZoom > minZoom.value) {
            zoomLevel.set(nextZoom)
        }
    }

    fun zoomLevelProperty(): ReadOnlyDoubleProperty = zoomLevel.readOnlyProperty
    fun maxZoomProperty(): ReadOnlyDoubleProperty = maxZoom.readOnlyProperty
    fun minZoomProperty(): ReadOnlyDoubleProperty = minZoom.readOnlyProperty
    fun zoomStepProperty(): ReadOnlyDoubleProperty = zoomStep.readOnlyProperty
    fun centerProperty(): ReadOnlyObjectProperty<Coordinate> = center.readOnlyProperty

    override fun getMapLayers(): List<IMapLayer> {
        return mapLayers
    }

    override fun addMapLayer(layer: IMapLayer) {
        mapLayers.add(layer)
    }

    override fun addMapLayer(index: Int, layer: IMapLayer) {
        mapLayers.add(index, layer)
    }

    override fun removeMapLayer(index: Int) {
        mapLayers.removeAt(index)
    }

    override fun removeMapLayer(layer: IMapLayer) {
        mapLayers.remove(layer)
    }

    override fun addMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener) {
        when (eventType) {
            MapLayerEventType.ALL -> {
                doAddMapLayerListener(MapLayerEventType.LAYER_ADDED, listener)
                doAddMapLayerListener(MapLayerEventType.LAYER_REMOVED, listener)
            }
            else -> {
                doAddMapLayerListener(eventType, listener)
            }
        }
    }

    override fun removeMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener) {
        when (eventType) {
            MapLayerEventType.ALL -> {
                doRemoveMapLayerListener(MapLayerEventType.LAYER_ADDED, listener)
                doRemoveMapLayerListener(MapLayerEventType.LAYER_REMOVED, listener)
            }
            else -> {
                doRemoveMapLayerListener(eventType, listener)
            }
        }
    }

    private fun doRemoveMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener) {
        mapLayerListeners.computeIfPresent(eventType) { _, listeners ->
            listeners.remove(listener)
            return@computeIfPresent listeners
        }
    }

    private fun doAddMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener) {
        mapLayerListeners.getOrPut(eventType, ::mutableListOf).add(listener)
    }

    private fun fireMapInvalidated() {
        observers.forEach {
            it.invalidated(this)
        }
    }
}
