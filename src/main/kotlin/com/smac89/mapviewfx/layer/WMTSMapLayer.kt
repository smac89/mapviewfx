package com.smac89.mapviewfx.layer

import com.smac89.mapviewfx.tile.ITileProvider
import javafx.beans.property.ReadOnlyStringProperty
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.scene.layout.TilePane
import org.geotools.ows.wmts.model.WMTSLayer

class WMTSMapLayer(private val layer: WMTSLayer): ITileLayer, IMapLayerFX, TilePane() {
    private val nameProperty = ReadOnlyStringWrapper(this, "name", layer.name)

    init {
    }

    override fun setTileProvider(tileProvider: ITileProvider) {
    }

    override fun nameProperty(): ReadOnlyStringProperty = nameProperty.readOnlyProperty

    override fun render() {
    }

    override fun getFXView() = this
}
