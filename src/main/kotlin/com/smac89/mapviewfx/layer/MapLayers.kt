package com.smac89.mapviewfx.layer

import com.smac89.mapviewfx.tile.ITileProvider
import javafx.beans.property.BooleanProperty
import javafx.beans.property.ReadOnlyDoubleProperty
import javafx.beans.property.ReadOnlyStringProperty
import javafx.scene.Node

interface IMapLayer {
    fun nameProperty(): ReadOnlyStringProperty
    fun visibleProperty(): BooleanProperty
    fun widthProperty(): ReadOnlyDoubleProperty
    fun heightProperty(): ReadOnlyDoubleProperty
    fun render()
}

interface ITileLayer: IMapLayer {
    fun setTileProvider(tileProvider: ITileProvider)
}

interface IMapLayerFX: IMapLayer {
    fun getFXView(): Node
}
