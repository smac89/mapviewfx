package com.smac89.mapviewfx.layer;

import java.util.*

@FunctionalInterface
interface MapLayerListener : EventListener {
    fun layerEvent(eventType: MapLayerEventType, layer: IMapLayer)
}

enum class MapLayerEventType {
    ALL, LAYER_ADDED, LAYER_REMOVED
}
