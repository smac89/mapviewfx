package com.smac89.mapviewfx.tile

import java.net.URL

interface ITileProvider {
    fun getTilesHeight(): Int
    fun getTilesWidth(): Int
    fun getTiles(width: Double, height: Double, zoomLevel: Double): Collection<Tile>
}

interface Tile {
    fun getUrl(): URL
    fun getTileIdentifier(): TileIdentifier

    companion object {
        const val DEFAULT_TILE_SIZE = 256.0
    }
}

data class TileIdentifier(val x: Double, val y: Double, val z: Double)
