package com.smac89.mapviewfx

import com.smac89.mapviewfx.layer.IMapLayer
import com.smac89.mapviewfx.layer.MapLayerEventType
import com.smac89.mapviewfx.layer.MapLayerListener
import javafx.beans.Observable
import org.locationtech.jts.geom.CoordinateXY

/**
 * An observable map.
 * Invalidation only occurs when a change happens with the map layers
 */
interface IMap: Observable {
    fun getCenterLatLon(): Coordinate
    fun getCenterXY(): CoordinateXY = MapUtilFx.lonLatToXY(getCenterLatLon())
    fun setCenterLatLon(latitude: Double, longitude: Double) = setCenterLatLon(Coordinate(latitude, longitude))
    fun setCenterLatLon(coordinate: Coordinate)
    fun setCenterXY(x: Double, y: Double) = setCenterLatLon(MapUtilFx.xyToLonLat(x, y))
    fun setCenterXY(coordinateXY: CoordinateXY) = setCenterLatLon(MapUtilFx.xyToLonLat(coordinateXY))

    fun getZoomLevel(): Double
    fun getMaxZoomLevel(): Double
    fun getMinZoomLevel(): Double
    fun setZoomLevel(zoomLevel: Double)
    fun setMaxZoomLevel(maxZoom: Double)
    fun setMinZoomLevel(minZoom: Double)
    fun getZoomStep(): Double
    fun setZoomStep(zoomStep: Double)
    fun zoomIn()
    fun zoomOut()

    fun addMapLayer(layer: IMapLayer)
    fun addMapLayer(vararg layers: IMapLayer) = layers.forEach(::addMapLayer)
    fun addMapLayer(index: Int, layer: IMapLayer)

    fun removeMapLayer(layer: IMapLayer)
    fun removeMapLayer(vararg layers: IMapLayer) = layers.forEach(::removeMapLayer)
    fun removeMapLayer(index: Int)

    fun getMapLayers(): List<IMapLayer>

    fun addMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener)

    fun removeMapLayerListener(eventType: MapLayerEventType, listener: MapLayerListener)
}

private typealias LayerEventHandler = (eventType: MapLayerEventType, layer: IMapLayer) -> Unit

fun IMap.addMapLayerListener(eventType: MapLayerEventType, handler: LayerEventHandler) =
        addMapLayerListener(eventType, object: MapLayerListener {
            override fun layerEvent(eventType: MapLayerEventType, layer: IMapLayer) = handler(eventType, layer)
        })
